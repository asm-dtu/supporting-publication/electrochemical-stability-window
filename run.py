from __future__ import annotations
import argparse
from dataclasses import dataclass
import logging
from os import environ
from pathlib import Path
from typing import List, Optional

from pymatgen.io.vasp import Vasprun
from pymatgen.entries.computed_entries import ComputedStructureEntry
from pymatgen.entries.compatibility import MaterialsProject2020Compatibility

from esw import DeltaPotential, calculate_ESW


@dataclass
class Arguments:
    """Container for parsed arguments

    Makes typing much easier.
    """
    directory: Path
    correction: float
    element: str
    exclusions: Optional[List[str]]
    force: bool
    log: str
    api_key: str
    plot: bool

    def __post_init__(self):
        if self.api_key is not None:
            return
        
        self.api_key = environ.get("MP_API_KEY", None)
        if self.api_key is None:
            raise LookupError(
                "API key not given in either input or environment "
                "variables! To use this script either supply your Materials "
                " Project API key with `--api_key` or set it using the "
                "environment variable `MP_API_KEY`."
            )



def parse_args() -> Arguments:
    """Parse the given CLI inputs"""
    parser = argparse.ArgumentParser(
        description="""
        Determine the Electrochemical Stability Window (ESW) from a VASP
        calculation using the Grand Potential Phase Diagram (GPPD) method.
        This takes as an input a path to a VASP calculation directory and the
        element to which the GPPD is open, and gives the upper and lower
        voltages of electrochemical stability.
        """
    )
    parser.add_argument(
        "element", type=str,
        help=("The Electrochemical Stability Window is calculated in "
              "regards to this element.")
    )
    parser.add_argument(
        "directory", type=Path, nargs='?', default=Path(),
        help=("The directory that contains the VASP calculation output. The "
              "directory must contain a valid `vasprun.xml` file")
    )
    parser.add_argument(
        "-corr", "--correction", type=float, default=0.0,
        help=("Change the total energy of the calculation by this amount. "
              "The value is interpreted as eV/atom.")
    )
    parser.add_argument(
        "--exclusions", type=str, nargs='+', default=[],
        help=("Exclude the following materials from the phase diagrams. "
              "This allows excluding certain decomposition products that "
              "are known to be kinetically inhibited.")
    )
    parser.add_argument(
        "-f", "--force_to_hull", dest="force", action="store_true",
        help=("Lower the total energy of the material such that it ends up "
              "on the convex hull. This is required for metastable "
              "materials to actually get a value for the ESW. Enabling this "
              "feature will log the energy above the hull.")
    )
    parser.add_argument(
        "--log", choices={"WARNING", "INFO", "DEBUG"}, default="INFO",
        help="Which level of severity to log output at."
    )
    parser.add_argument(
        "--api_key", type=str, default=None,
        help=("The API key to search the Material Project database. This "
        "uses their next-gen interface, so legacy keys are not accepted. If "
        "the environment variable `MP_API_KEY` is not set, this parameter "
        "is required.")
    )
    parser.add_argument(
        '--plot', action="store_true",
        help="Plot the phase diagram and GPPDs while determining the ESW."
    )

    return Arguments(**vars(parser.parse_args()))

def extract_to_entry(directory: Path) -> ComputedStructureEntry:
    """Get the computed entry from a VASP calculation directory

    Extracts the completed run into a pymatgen data structure, and apply their
    2020 corrections.

    Parameters
    ----------
    directory : pathlib.PurePath
        A path to the directory that contains the completed VASP run, i.e.,
        the vasprun.xml file.

    Returns
    -------
    pymatgen.entries.computed_entries.ComputedStructureEntry
        A data format that allows easy manipulation and access to important
        values for the calculation of the ESW.
    """
    try:
        entry = Vasprun(directory / 'vasprun.xml').get_computed_entry()
    except FileNotFoundError:
        logging.error("Given directory does not contain a `vasprun.xml` file.")
        raise
    compat = MaterialsProject2020Compatibility()
    return compat.process_entry(entry) or entry  # function can return None

def determine_esw(
    entry: ComputedStructureEntry,
    element: str,
    correction: float = 0.0,
    exclusions: Optional[List[str]] = None,
    force_to_hull: bool = False,
    api_key: str = "",
    plot: bool = False
) -> DeltaPotential:
    """Determine the Electrochemical Stability Window (ESW)
    
    Uses the Grand Potential Phase Diagram (GPPD) to calculate the ESW of a
    bulk material with the ability to force the material onto the convex
    hull. Additionally, decomposition products can be excluded from the phase
    space, e.g., if there is a known kinetic inhibition towards decomposition
    to that material.

    Parameters
    ----------
    entry : pymatgen.entries.computed_entries.ComputedStructureEntry
        Represents the result of the VASP calculation
    element : str
        The open element whose chemical potential is changed in the GPPD.
    correction : float, optional
        Use this to change the total energy of the entry. The value is
        interpreted as eV/at.
    exclusions : list of str
        Materials to exclude from the phase diagrams. The chemical formula
        must match the ways it's given in the Materials Project.
    force_to_hull : bool
        If True, the total energy of the material is lowered such that it
        sits on the convex hull of its phase diagram. This does result in a
        metastable ESW, but for materials above the hull, the calculated ESW
        is generally 0, so this can give an actual ESW for materials that are
        only slightly metastable.

    Returns
    -------
    esw.DeltaPotential
        A dataclass that contains the upper and lower voltage of the
        stability window and its range/size.
    """
    entry._energy += correction * len(entry.structure)
    return calculate_ESW(
        element, entry, exclusions, force_to_hull, api_key, plot
    )

def main():
    args = parse_args()
    logging.basicConfig(level=getattr(logging, args.log))

    try:
        entry = extract_to_entry(args.directory)
    except FileNotFoundError:
        quit()

    del_pot = determine_esw(
        entry, args.element, args.correction, args.exclusions, args.force,
        args.api_key
    )

    print(entry.name, del_pot)


if __name__ == '__main__':
    main()
