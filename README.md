# Electrochemical Stability Window
This repository contains code to calculate the electrochemical stability window of a material based on a VASP calculation at the PBE(+U) level, as long as the parameters align with those of the [Material Project](https://docs.materialsproject.org/methodology/materials-methodology/calculation-details/gga+u-calculations).

The code is based on the Grand Potential Phase Diagram method (see [Mo _et al._](https://doi.org/10.1021/cm203303y), [Zhu _et al._](https://doi.org/10.1021/acsami.5b07517), and [Ong _et al._](https://doi.org/10.1016/j.elecom.2010.01.010)) and uses the Materials Project database of computational materials to calculate the phase diagrams of chemical systems.


## Getting started
The code is written in `python3` and is dependent on `numpy` and the Materials Project API package, `mp_api`. Make sure you have both installed before running the code.z

To install the code, simply clone this project to yourself using
```
git clone https://gitlab.com/asm-dtu/supporting-publication/electrochemical-stability-window.git
```

## Usage
To use the code you need an API key for the Materials Project, which you can get [here](https://next-gen.materialsproject.org/api). Please note, that this code uses their new API, so DO NOT go for their _legacy API_.  
Once you have your API key, you can then either set it as the environment variable `MP_API_KEY`, or you can give it directly to the script with
```
python3 run.py element [directory] --api_key <YOUR MP API KEY>
```

The script has a command line interface, so all the options can be found by calling
```
python3 run.py --help
```

## Authors and acknowledgment
This code has been developed by Benjamin H. Sjølin, and makes use of the [Materials Project API](https://github.com/materialsproject/api) to function.
