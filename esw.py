from __future__ import annotations
from collections import defaultdict
from functools import partial
import logging
from math import ceil
from typing import Iterable, List, Optional, Tuple
from warnings import filterwarnings

from emmet.core.thermo import ThermoType
from mp_api.client.mprester import MPRester, DEFAULT_API_KEY
from numpy import inf, ndarray, ones, convolve, zeros_like, float64
from pymatgen.analysis.phase_diagram import (
    GrandPotentialPhaseDiagram,
    PDPlotter,
    PhaseDiagram,
)
from pymatgen.core.periodic_table import Element
from pymatgen.entries.compatibility import (
    AnyComputedEntry,
    ComputedStructureEntry
)


filterwarnings('ignore', message=".*mpcontribs-client not installed.*")


# --- Classes ---
class DeltaPotential:
    """Wrapper class to contain and easily update an Electrochemical
    Stability Window (ESW). Keeps track of minimum and maximum potential of
    stability, and determine the window size from that."""

    def __init__(self) -> None:
        self._min = inf
        self._max = -inf

    def update(self, min_val: float, max_val: float) -> None:
        """Expand the limits of the ESW

        The call will automatically take care that the window can only become
        larger, so will not overwrite the min and max if they don't become
        more negative or positive, respectively.

        Parameters
        ----------
        min_val : float
            Possible new lower limit of ESW
        max_val : float
            Possible new upper limit of ESW
        """
        self.min = min_val
        self.max = max_val

    def values(self) -> Tuple[float, float]:
        """Return the limits of the ESW

        Returns
        -------
        tuple of three floats
            The lower and upper limit of the recorded ESW, and its size
        """
        return (self.min, self.max, self.range)

    @property
    def min(self):
        return self._min

    @min.setter
    def min(self, value):
        self._min = min(self._min, value)

    @property
    def max(self):
        return self._max

    @max.setter
    def max(self, value):
        self._max = max(self._max, value)

    @property
    def range(self) -> float:
        """The size of the ESW"""
        if self.max == -inf and self.min == inf:
            return 0
        return self.max - self.min

    def __repr__(self) -> str:
        """Used for displaying the ESW"""
        return (
            f"{self.__class__.__name__}(min={self.min:.2f}, "
            f"max={self.max:.2f}, range={self.range:.2f})"
        )


# --- Utilities ---
def remove_duplicates(
    entries: Iterable[ComputedStructureEntry],
) -> List[ComputedStructureEntry]:
    """Remove all but the most stable entry for each reduced formula

    Groups the entries by reduced formula and removes all but the most stable
    entry for each group.

    Parameters
    ----------
    entries : iterable with `ComputedStructureEntry`'s
        The non-unique entries to reduce

    Returns
    -------
    list of `ComputedStructureEntry`
        The most stable entry for each unique reduced formula
    """
    # Group by reduced composition
    entries_dict = defaultdict(list)
    for entry in entries:
        entries_dict[entry.composition.reduced_formula].append(
            (entry, entry.energy_per_atom)
        )

    # Filter based on energy_per_atom - keep most stable entries
    return [min(cat_es, key=lambda e: e[1])[0]
            for cat_es in entries_dict.values()]


def remove_exclusions(
    entries: Iterable[ComputedStructureEntry], exclusions: Iterable[str]
) -> List[ComputedStructureEntry]:
    """Exclude certain entries from the input

    Excludes entries based on 2 criteria:
        1) Their material ID is in `exclusions`.

        2) Their reduced formula is in `exclusions`.

    Parameters
    ----------
    entries : iterable with `ComputedStructureEntry`'s
        An iterable contianer with entries, some of which should be excluded
    exclusions : iterable with str's
        The (reduced) formulas and material IDs to exclude

    Returns
    -------
    list of `ComputedStructureEntry`
        The input with the excluded entries removed

    Notes
    -----
    This function is case-sensitive, so material IDs must be `mp-<integer>`,
    and formulas must be `<Element>[<integer>]<Element>[<integer>]...`, e.g.,
    `Mg3As2`. NB: The order of elements has to match the standard order
    defined by `pymatgen` for the exclusion to take effect.
    """

    def inclusion_filter(
        entry: ComputedStructureEntry, exclusions: Iterable[str]
    ) -> bool:
        """Return whether the entry should be included"""
        for exclusion in exclusions:
            # Assumes `entry.entry_id` has form `mp-<integer>-{GGA,R2SCAN}`
            if entry.entry_id.rpartition("-")[0] == exclusion:
                return False
            if entry.composition.reduced_formula == exclusion:
                return False
        return True

    # Fill out the exclusions such that we get a single input function
    inc_f = partial(inclusion_filter, exclusions=exclusions)

    return list(filter(inc_f, entries))


def get_chemical_system(
    entry: AnyComputedEntry,
    exclusions: List[str],
    api_key: Optional[str] = DEFAULT_API_KEY,
) -> List[ComputedStructureEntry]:
    """Collect the chemical system entries for `entry`

    Grab all the most stable GGA(+U) entries from Materials Project that are
    part of the chemical system spanned by the elements of `entry`. Adds
    `entry` to the output.

    Parameters
    ----------
    entry : `ComputedEntry` or `ComputedStructureEntry`
        The entry that defines the chemical system to search in. Any entries
        found with the same reduced formula as the entry are excluded, such
        that this entry is seen as the most stable
    exclusions : list of str
        A list of (reduced) formulas and material IDs to exclude from the
        search
    api_key : str, optional
        Your Materials Project API key (non-legacy). Can also be set by the
        environment variable `MP_API_KEY`

    Returns
    -------
    list of `ComputedStructureEntry`
        All the most stable entries in the chemical system spanned by `entry`
        with `entry` itself as the last element
    """
    with MPRester(api_key) as mpr:
        # Get only the GGA(+U) entries. Will mix in R2SCAN entries otherwise
        entries = mpr.get_entries_in_chemsys(
            entry.composition.chemical_system,
            additional_criteria={"thermo_types": [ThermoType.GGA_GGA_U]}
        )

    # Ensure that `entry` is seen as the most stable of its kind
    exclusions.append(entry.composition.reduced_formula)

    # Remove non-needed entries
    entries = remove_duplicates(entries)
    entries = remove_exclusions(entries, exclusions)

    # Add the supplied entry
    entries.append(entry)
    return entries


def moving_average(x: ndarray, offset: float = 0.1) -> ndarray:
    """Calculate the moving average with width 2

    Determines the moving average of the input, where each average is based
    off just the 2 adjacent points. To ensure the same output size as the
    input, the last value is calculated from just the last input value, with
    a user-defined offset subtracted.

    Parameters
    ----------
    x : `numpy.ndarray`
        A 1D array of values
    offset : float, optional
        Value subtracted from the last input value.

    Returns
    -------
    `numpy.ndarray`
        A 1D array with the width 2 moving average of the input
    """
    res = zeros_like(x, dtype=float64)
    res[:-1] = convolve(x, ones(2) / 2, "valid")
    res[-1] = x[-1] - offset
    return res


def apply_hull_correction(
    entry: ComputedStructureEntry, e_above_hull: float
) -> None:
    """Lower the entry onto the convex hull and log it."""
    if e_above_hull > 0.0:
        entry._energy -= ceil(e_above_hull * 100)/100 * len(entry.structure)
        logging.info(
            f"Reduced energy of material with {e_above_hull:.3f} eV/atom."
        )
    else:
        logging.info("Material is on the hull.")


# --- Electrochemistry functions ---
def calculate_e_above_hull(
    entry: ComputedStructureEntry, entries: List[ComputedStructureEntry]
) -> float:
    """Calculate the energy above the hull for a given material

    Uses the `entries` to build the phase space and then calculates how far
    above the convex hull `entry` is. If the material is on the hull, this
    function returns 0.

    Parameters
    ----------
    entry : ComputedStructureEntry
        The material to calculate the energy above the hull for.
    entries : list of ComputedStructureEntry
        The materials of the phase space in the chemical system spanned by
        the `entry`.

    Returns
    -------
    float
        The energy above the hull in eV/atom. If the material is on the hull,
        0 is returned.

    Notes
    -----
    `entries` must contain `entry` for this function to work as designed.
    """
    pd = PhaseDiagram(entries)
    return pd.get_e_above_hull(entry)


def calculate_ESW(
    open_element: str,
    entry: ComputedStructureEntry,
    exclusions: List[str],
    force_to_hull: bool = False,
    api_key: Optional[str] = DEFAULT_API_KEY,
    plot: bool = False
) -> DeltaPotential:
    """Calculate the Electrochemical Stability Window

    Calculates the electrochemical stability window of a given computed entry
    with respect to a specified element in the entry. This function uses the
    method of the Grand Potential Phase Diagram to determine the window.

    Parameters
    ----------
    open_element : str
        The element whose chemical potential is assumed free
    entry : `ComputedEntry` or `ComputedStructureEntry`
        The data from a calculation. Contains the composition and energy
    exclusions : list of str's, optional
        Reduced formulas or material IDs to ignore/remove from the chemical
        system
    force_to_hull : bool, optional
        If set the material will be moved down onto the convex hull. The 
        function will report by how much the energy was changed, if at all.
    api_key : str, optional
        Your Materials Project API key (non-legacy). Can also be set by the
        environment variable `MP_API_KEY`
    plot : bool, optional
        Use to plot the ordinary and all the grand potential phase diagrams

    Returns
    -------
    `DeltaPotential`
        The electrochemical stability window in a wrapper class
    """
    entries = get_chemical_system(entry, [], api_key)

    if force_to_hull:
        e_above_the_hull = calculate_e_above_hull(entry, entries)
        apply_hull_correction(entry, e_above_the_hull)

    entries = remove_exclusions(entries, exclusions)

    pd = PhaseDiagram(entries)
    if plot:
        pd_plot = PDPlotter(pd)
        pd_plot.show()

    # Transform input for it to get understood
    open_element = Element(str(open_element))
    chempots = pd.get_transition_chempots(open_element)

    # Find all the midpoints of the critical chemical potentials
    average_chemical_potentials = moving_average(chempots, 2)
    chempots = (*chempots, -inf)
    logging.debug(f"Transition chemical potentials: {chempots}")

    delta_potential = DeltaPotential()
    # Generate a GPPD at each midpoint, and determine whether the entry is
    # stable in this range
    for i, mu in enumerate(average_chemical_potentials):
        gppd = GrandPotentialPhaseDiagram(
            entries, {open_element: mu}, pd.elements
        )
        if entry.name in [e.name for e in gppd.stable_entries]:
            delta_potential.update(chempots[i + 1], chempots[i])

        if plot:
            gppd_plot = PDPlotter(gppd)
            gppd_plot.show()

        logging.debug(
            f"Stable species in range [{chempots[i]:.4f}, "
            f"{chempots[i+1]:.4f}]: {[e.name for e in gppd.stable_entries]}"
        )

    return delta_potential
